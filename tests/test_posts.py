from unittest.mock import MagicMock, patch

import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import models


@pytest.fixture
def setup_data(test_db: Session):
    # Create test posts
    post1 = models.Post(
        caption="Test Caption 1",
        creator="Test Creator 1",
        image="test_image1.jpg",
    )
    post2 = models.Post(
        caption="Test Caption 2",
        creator="Test Creator 2",
        image="test_image2.jpg",
    )

    test_db.add(post1)
    test_db.add(post2)
    test_db.commit()

    # Create test comments
    comment1 = models.Comment(
        content="Test Comment 1", creator="Commenter 1", post_id=post1.id
    )
    comment2 = models.Comment(
        content="Test Comment 2", creator="Commenter 2", post_id=post1.id
    )
    comment3 = models.Comment(
        content="Test Comment 3", creator="Commenter 3", post_id=post2.id
    )
    comment4 = models.Comment(
        content="Test Comment 4", creator="Commenter 4", post_id=post2.id
    )
    comment5 = models.Comment(
        content="Test Comment 5", creator="Commenter 5", post_id=post2.id
    )

    test_db.add(comment1)
    test_db.add(comment2)
    test_db.add(comment3)
    test_db.add(comment4)
    test_db.add(comment5)
    test_db.commit()


def test_read_posts(client: TestClient, setup_data):
    response = client.get("/posts/?skip=0&limit=10")
    assert response.status_code == 200
    data = response.json()["data"]
    assert len(data) == 2

    # Check the first post
    assert data[0]["caption"] == "Test Caption 2"
    assert data[0]["creator"] == "Test Creator 2"
    assert data[0]["total_comments"] == 3
    assert len(data[0]["comments"]) == 2

    # Check the second post
    assert data[1]["caption"] == "Test Caption 1"
    assert data[1]["creator"] == "Test Creator 1"
    assert data[1]["total_comments"] == 2
    assert len(data[1]["comments"]) == 2


def test_read_posts_pagination(client: TestClient):
    response = client.get("/posts/?skip=0&limit=1")
    assert response.status_code == 200
    assert len(response.json()["data"]) == 1

    pagination = response.json()["pagination"]
    assert pagination["previous"] is None
    assert pagination["next"]

    # Check pagination actually works
    response = client.get(pagination["next"])
    assert response.status_code == 200
    assert len(response.json()["data"]) == 1

    pagination = response.json()["pagination"]
    assert pagination["next"] is None
    assert pagination["previous"]


@patch("app.storage_strategy.get_storage_strategy")
def test_create_post(
    mock_get_storage_strategy, client: TestClient, test_db: Session
):
    mock_strategy = MagicMock()
    mock_strategy.save_image.return_value = "test_image.jpg"
    mock_strategy.convert_and_resize_image.return_value = (
        "test_image_thumbnail.jpg"
    )
    mock_get_storage_strategy.return_value = mock_strategy

    response = client.post(
        "/posts/",
        files={"file": ("test_image.jpg", b"test_image_content")},
        data={"caption": "Test Caption", "creator": "Test Creator"},
    )

    assert response.status_code == 200
    data = response.json()
    assert data["caption"] == "Test Caption"
    assert data["creator"] == "Test Creator"
    assert data["image"] == "test_image_thumbnail.jpg"

    post = (
        test_db.query(models.Post).filter(models.Post.id == data["id"]).first()
    )
    assert post


def test_create_post_extension_validation(client: TestClient):
    response = client.post(
        "/posts/",
        files={"file": ("test_image.svg", b"test_image_content")},
        data={"caption": "Test Caption", "creator": "Test Creator"},
    )

    assert response.status_code == 400
    assert response.json() == {"detail": "Invalid image format"}
