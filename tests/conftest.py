import pytest
from fastapi.testclient import TestClient

from app import main
from app.database import Base, SessionLocal, engine, get_db


@pytest.fixture(scope="session")
def test_db():
    Base.metadata.create_all(bind=engine)
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
        Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope="session")
def client(test_db):
    def override_get_db():
        try:
            yield test_db
        finally:
            test_db.close()

    main.app.dependency_overrides[get_db] = override_get_db
    with TestClient(main.app) as test_client:
        yield test_client
