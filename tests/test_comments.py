import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import models


@pytest.fixture
def setup_post(test_db: Session):
    post = models.Post(
        caption="Test Post", creator="Test Creator", image="test_image.jpg"
    )
    test_db.add(post)
    test_db.commit()
    yield post
    test_db.delete(post)
    test_db.commit()


@pytest.fixture
def setup_comment(test_db: Session, setup_post: models.Post):
    comment = models.Comment(
        content="Test Comment 1", creator="Commenter", post_id=setup_post.id
    )
    test_db.add(comment)
    test_db.commit()
    yield comment
    test_db.query(models.Comment).delete()
    test_db.commit()


def test_create_comment(client: TestClient, setup_post: models.Post):
    post_id = setup_post.id
    response = client.post(
        f"/comments/{post_id}",
        json={"content": "New Comment", "creator": "New Commenter"},
    )
    assert response.status_code == 200
    data = response.json()
    assert data["content"] == "New Comment"
    assert data["creator"] == "New Commenter"
    assert data["post_id"] == post_id


def test_delete_comment(client: TestClient, setup_comment):
    response = client.delete(
        f"/comments/{setup_comment.id}?creator={setup_comment.creator}"
    )
    assert response.status_code == 200
    assert response.json() == {"message": "Comment deleted successfully"}


def test_delete_from_other_creator(client: TestClient, setup_comment):
    comment_id = setup_comment.id
    creator = "random_creator"
    response = client.delete(f"/comments/{comment_id}/?{creator=}")
    assert response.status_code == 404
    assert response.json() == {"detail": "Comment not found"}


def test_delete_non_existent_comment(client: TestClient):
    response = client.delete("/comments/999?creator=foo")
    assert response.status_code == 404
    assert response.json() == {"detail": "Comment not found"}
