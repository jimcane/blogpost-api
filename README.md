# BlogPost API

BlogPost API is a FastAPI-based RESTful API for managing posts and comments.

## Prerequisites

- Python 3.12
- Docker
- Docker Compose

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/jimcane/blogpost-api.git
   ```

2. Navigate to the project directory:

   ```bash
   cd blogpost-api
   ```

3. Install dependencies using [Poetry](https://python-poetry.org/docs/#installation):

   ```bash
   poetry install
   ```

## Usage

### Running the API

1. Start the PostgreSQL database and the FastAPI application using Docker Compose:

   ```bash
   docker-compose up
   ```

2. The API should now be accessible at `http://localhost:8000`.

### Running Tests

1. Build and run the tests using Docker Compose:

   ```bash
   docker-compose --profile test run test
   ```

## API Documentation

API documentation and interactive testing can be accessed at `http://localhost:8000/docs`.

## Environment Variables

`MAX_UPLOAD_SIZE`: Maximum allowed size for image uploads in bytes (default: 104857600, equivalent to 100 MB).

## License

This project is licensed under the MIT License - see the `LICENSE.txt` for details.
