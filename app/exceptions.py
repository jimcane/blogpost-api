class StorageConfigException(Exception):
    pass


class CommentException(Exception):
    pass


class CommentDoesNotExist(CommentException):
    pass
