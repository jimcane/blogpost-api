from fastapi import FastAPI

from app.config import get_max_upload_size
from app.database import Base, engine
from app.middleware import LimitUploadSizeMiddleware
from app.routers import comments, posts

Base.metadata.create_all(bind=engine)

app = FastAPI()

app.add_middleware(
    LimitUploadSizeMiddleware, max_upload_size=get_max_upload_size()
)

app.include_router(posts.router)
app.include_router(comments.router)
