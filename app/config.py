import os


def get_max_upload_size(default: int = 100 * 1024**2) -> int:
    """Max upload size by default is 100 MB"""
    return int(os.getenv("MAX_UPLOAD_SIZE", default))
