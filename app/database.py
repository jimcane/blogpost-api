import os

from sqlalchemy import MetaData, create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

DATABASE_URL = "postgresql://postgres:password@db:5432/devdb"
TEST_DATABASE_URL = "sqlite:///./test.db"

IS_TESTING = os.getenv("TESTING", "0") == "1"

engine = create_engine(TEST_DATABASE_URL if IS_TESTING else DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()
metadata = MetaData()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
