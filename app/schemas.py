from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, ConfigDict


class CommentBase(BaseModel):
    content: str
    creator: str


class CommentCreate(CommentBase):
    pass


class Comment(CommentBase):
    id: int
    created_at: datetime
    post_id: int

    model_config = ConfigDict(from_attributes=True)


class PostBase(BaseModel):
    caption: str
    creator: str


class PostCreate(PostBase):
    pass


class Post(PostBase):
    id: int
    image: str
    created_at: datetime
    comments: List[Comment] = []
    total_comments: int = 0

    model_config = ConfigDict(from_attributes=True)


class Pagination(BaseModel):
    previous: Optional[str]
    next: Optional[str]


class PaginatedPosts(BaseModel):
    data: List[Post]
    pagination: Pagination
