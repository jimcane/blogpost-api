from fastapi import HTTPException
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.requests import Request


class LimitUploadSizeMiddleware(BaseHTTPMiddleware):
    def __init__(self, app, max_upload_size: int):
        super().__init__(app)
        self.max_upload_size = max_upload_size
        self.max_upload_size_mb = self.max_upload_size // (1024**2)

    async def dispatch(self, request: Request, call_next):
        if request.headers.get("content-length"):
            content_length = int(request.headers["content-length"])
            if content_length > self.max_upload_size:
                raise HTTPException(
                    status_code=413,
                    detail=f"File size exceeds maximum limit of {self.max_upload_size_mb}MB",
                )
        response = await call_next(request)
        return response
