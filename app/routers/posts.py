from urllib.parse import urlencode

from fastapi import (
    APIRouter,
    Depends,
    File,
    Form,
    HTTPException,
    Request,
    UploadFile,
)
from sqlalchemy.orm import Session

from app import crud, schemas, storage_strategy
from app.database import get_db
from app.exceptions import StorageConfigException


router = APIRouter(
    prefix="/posts",
    tags=["posts"],
)


@router.post("/", response_model=schemas.Post)
def create_post(
    caption: str = Form(...),
    creator: str = Form(...),
    file: UploadFile = File(...),
    db: Session = Depends(get_db),
):

    print(f"{file.content_type = }")
    if file.content_type not in ["image/png", "image/jpeg", "image/bmp"]:
        raise HTTPException(status_code=400, detail="Invalid image format")

    try:
        storage = storage_strategy.get_storage_strategy(file=file)
    except StorageConfigException as e:
        raise HTTPException(status_code=500, detail=e)

    img_path = storage.save_image()
    jpg_img_path = storage.convert_and_resize_image(image_path=img_path)

    return crud.create_post(
        db=db,
        post=schemas.PostCreate(caption=caption, creator=creator),
        image=jpg_img_path,
    )


def construct_url(request: Request, skip: int, limit: int) -> str:
    base_url = str(request.url).split("?")[0]
    query_params = {"skip": skip, "limit": limit}
    return f"{base_url}?{urlencode(query_params)}"


@router.get("/", response_model=schemas.PaginatedPosts)
def read_posts(
    request: Request,
    skip: int = 0,
    limit: int = 10,
    db: Session = Depends(get_db),
):
    """
    Display all Posts, sorted by amount of comments but only returns
    last two comments and with pagination
    """

    posts = crud.get_posts_with_last_two_comments(db, skip=skip, limit=limit)
    total_posts = crud.get_total_posts(db)

    previous_url = (
        construct_url(request=request, skip=max(skip - limit, 0), limit=limit)
        if skip > 0
        else None
    )
    next_url = (
        construct_url(request=request, skip=skip + limit, limit=limit)
        if skip + limit < total_posts
        else None
    )

    return schemas.PaginatedPosts(
        data=posts,
        pagination=schemas.Pagination(previous=previous_url, next=next_url),
    )
