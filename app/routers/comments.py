from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, exceptions, schemas
from app.database import get_db

router = APIRouter(
    prefix="/comments",
    tags=["comments"],
)


@router.post("/{post_id}", response_model=schemas.Comment)
def create_comment(
    post_id: int, comment: schemas.CommentCreate, db: Session = Depends(get_db)
):
    return crud.create_comment(db=db, comment=comment, post_id=post_id)


@router.delete("/{comment_id}")
def delete_comment(
    comment_id: int, creator: str, db: Session = Depends(get_db)
):
    try:
        print(f"{comment_id = } {creator = }")
        crud.delete_comment(db=db, comment_id=comment_id, creator=creator)
    except exceptions.CommentException:
        raise HTTPException(status_code=404, detail="Comment not found")

    return {"message": "Comment deleted successfully"}
