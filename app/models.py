import datetime

from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.database import Base


class Post(Base):
    __tablename__ = "posts"

    id = Column(Integer, primary_key=True, index=True)
    caption = Column(String, index=True)
    image = Column(String)
    creator = Column(String)
    created_at = Column(
        DateTime, default=datetime.datetime.now(tz=datetime.UTC)
    )
    comments = relationship("Comment", back_populates="post")


class Comment(Base):
    __tablename__ = "comments"

    id = Column(Integer, primary_key=True, index=True)
    content = Column(String)
    creator = Column(String)
    created_at = Column(
        DateTime, default=datetime.datetime.now(tz=datetime.UTC)
    )
    post_id = Column(Integer, ForeignKey("posts.id"))

    post = relationship("Post", back_populates="comments")
