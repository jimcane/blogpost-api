import io
import os
import uuid
from abc import ABC, abstractmethod

import boto3
from botocore.exceptions import ClientError, NoCredentialsError
from fastapi import UploadFile
from PIL import Image

from app import exceptions


class StorageStrategy(ABC):
    THUMBNAIL_SIZE = (600, 600)

    def __init__(self, file: UploadFile):
        self.image_file = file

    @abstractmethod
    def save_image(self) -> str:
        pass

    @abstractmethod
    def convert_and_resize_image(self, image_path: str) -> str:
        pass


class LocalStorageStrategy(StorageStrategy):
    def save_image(self) -> str:
        images_dir = "/app/images"
        if not os.path.exists(images_dir):
            os.makedirs(images_dir)

        image_path = f"images/{uuid.uuid4()}{self.image_file.filename}"

        with open(image_path, "wb") as img:
            img.write(self.image_file.file.read())

        return image_path

    def convert_and_resize_image(self, image_path: str) -> str:
        with Image.open(self.image_file.file) as img:
            img = img.convert("RGB")
            img.thumbnail(self.THUMBNAIL_SIZE)
            jpg_img_path = f"images/{os.path.splitext(os.path.basename(image_path))[0]}_thumbnail.jpg"
            img.save(jpg_img_path, "JPEG")
        return jpg_img_path


class S3StorageStrategy(StorageStrategy):
    def __init__(
        self,
        file: UploadFile,
        bucket_name: str,
        aws_access_key_id: str,
        aws_secret_access_key: str,
        region_name: str,
    ):
        super().__init__(file=file)

        self.s3 = boto3.client(
            "s3",
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name=region_name,
        )
        self.bucket_name = bucket_name
        
        try:
            self.s3.head_bucket(Bucket=bucket_name)
        except (NoCredentialsError, ClientError) as e:
            raise exceptions.StorageConfigException('AWS credentials not found') from e

    def save_image(self) -> str:
            image_key = f"images/{uuid.uuid4()}{self.image_file.filename}"
            self.s3.upload_fileobj(
                self.image_file.file, self.bucket_name, image_key
            )
            return f"s3://{self.bucket_name}/{image_key}"

    def convert_and_resize_image(self, image_path: str) -> str:
        with Image.open(self.image_file.file) as img:
            img = img.convert("RGB")
            img.thumbnail(self.THUMBNAIL_SIZE)

            img_byte_arr = io.BytesIO()
            img.save(img_byte_arr, "JPEG")
            img_byte_arr.seek(0)

        jpg_img_key = f'{image_path.split('/')[-1].split('.')[0]}_thumbnail.jpg'
        self.s3.upload_fileobj(img_byte_arr, self.bucket_name, jpg_img_key)
        return f"s3://{self.bucket_name}/{jpg_img_key}"


def get_storage_strategy(file: UploadFile) -> StorageStrategy:
    storage_type = os.getenv("STORAGE_TYPE", "local")
    if storage_type == "s3":
        return S3StorageStrategy(
            file=file,
            bucket_name=os.getenv("AWS_S3_BUCKET", ""),
            aws_access_key_id=os.getenv("AWS_ACCESS_KEY_ID", ""),
            aws_secret_access_key=os.getenv("AWS_SECRET_ACCESS_KEY", ""),
            region_name=os.getenv("AWS_REGION", ""),
        )
    return LocalStorageStrategy(file=file)
