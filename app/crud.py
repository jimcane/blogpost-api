from sqlalchemy import desc, func
from sqlalchemy.orm import Session

from app import exceptions, models, schemas


def get_posts_with_last_two_comments(
    db: Session, skip: int = 0, limit: int = 10
) -> list[schemas.Post]:
    posts = (
        db.query(
            models.Post, func.count(models.Comment.id).label("total_comments")
        )
        .outerjoin(models.Comment)
        .group_by(models.Post.id)
        .order_by(desc("total_comments"))
        .offset(skip)
        .limit(limit)
        .all()
    )

    result = []
    for post, total_comments in posts:
        last_two_comments = (
            db.query(models.Comment)
            .filter(models.Comment.post_id == post.id)
            .order_by(models.Comment.created_at.desc())
            .limit(2)
            .all()
        )
        post.comments = last_two_comments
        post.total_comments = total_comments
        result.append(post)

    return result


def create_post(db: Session, post: schemas.PostCreate, image: str):
    db_post = models.Post(
        caption=post.caption, creator=post.creator, image=image
    )
    db.add(db_post)
    db.commit()
    db.refresh(db_post)
    return db_post


def get_total_posts(db: Session) -> int:
    return db.query(models.Post).count()


def create_comment(db: Session, comment: schemas.CommentCreate, post_id: int):
    db_comment = models.Comment(**comment.model_dump(), post_id=post_id)
    db.add(db_comment)
    db.commit()
    db.refresh(db_comment)
    return db_comment


def delete_comment(db: Session, comment_id: int, creator: str):
    db_comment = (
        db.query(models.Comment).filter(models.Comment.id == comment_id).first()
    )
    if not db_comment:
        raise exceptions.CommentDoesNotExist(f"{comment_id = } does not exist")
    if db_comment.creator != creator:
        raise exceptions.CommentException(
            f"comment do not belong to {creator = }"
        )

    db.delete(db_comment)
    db.commit()
